import React from "react";
import {Balances as BalancesUI} from "../../lib/ui";
import {Balances as BalancesContainer} from "../../lib/containers";
import Button from "react-bootstrap/Button";

const renderErrorPage = (getCurrencies: () => void) => {
  return (
    <div>
      Here's error layout
      <Button className="me-5" variant="link" onClick={() => getCurrencies()} >GET CURRENCIES</Button>
    </div>
  )
}
export const Balances = () => {
  return (
    // pass necessary data to UI lib component
    <BalancesContainer
      renderLoading={() => <div>Here's loading layout for Balances</div>}
      renderErrorPage={renderErrorPage}
      renderBalances={(props) => <BalancesUI balances={props.balances} fetchBalancesWithError={props.fetchBalancesWithError}/>}>
      {({Balances}) => (
        <>
          {Balances} {/* // include Layout component if needed*/}
        </>
      )}
    </BalancesContainer>)
}

