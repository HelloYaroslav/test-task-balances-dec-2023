import React from "react";
import {Authentication as AuthenticationUI} from "../../lib/ui/Forms";
import {AuthPage as AuthPageContainer} from "../../lib/containers";

interface Props {
  children: React.ReactElement
}

export const Authentication = () => {
  return (
    // pass necessary data to UI lib component
    <AuthPageContainer signIn renderAuthPage={(props) => <AuthenticationUI onSubmit={props.signIn} errors={props.errors} isLoading={props.isLoading}/>}>
      {({AuthPage}) => (
        <>
          {AuthPage} {/* // include Layout component if needed*/}
        </>
      )}
    </AuthPageContainer>)
}

