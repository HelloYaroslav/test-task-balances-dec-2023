import React from "react";
import {AuthPage as AuthPageContainer} from "../../lib/containers";
import {Registration as RegistrationUI} from "../../lib/ui/Forms";

interface Props {
  children: React.ReactElement
}

export const Registration = () => {
  return (
    // pass necessary data to UI lib component
    <AuthPageContainer signUp renderAuthPage={(props) => <RegistrationUI onSubmit={props.signUp} errors={props.errors} isLoading={props.isLoading}/>}>
      {({AuthPage}) => (
        <>
          {AuthPage} {/* // include Layout component if needed*/}
        </>
      )}
    </AuthPageContainer>)
}
