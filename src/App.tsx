import React, {createContext} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import {BrowserRouter as Router, Route, Routes} from "react-router-dom";
import {useAuth} from "./lib/hooks/useAuthorization";
import {Authentication, Balances, Registration} from "./pages";
import {Page} from "./lib/containers";

export const AuthContext = createContext<ReturnType<typeof useAuth>>(null as never);

const PageWrappers = (props: {children: React.ReactElement}) => {
  return <Page>{props.children}</Page>
}

function App() {
  const auth = useAuth()

  return (
    <AuthContext.Provider value={auth}>
      <Router>
        <Routes>
          <Route
            path="/registration"
            element={<Registration/>}
          />
          <Route
            path="/authentication"
            element={<Authentication/>}
          />
          <Route
            path="/"
            element={<PageWrappers><Balances/></PageWrappers>}
          />
        </Routes>
      </Router>
    </AuthContext.Provider>
  );
}

export default App;
