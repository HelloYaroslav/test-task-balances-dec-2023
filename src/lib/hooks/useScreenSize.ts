import {useLayoutEffect, useState} from 'react';

export const useScreenSize = () => {
  const [screenWidth, setScreenWidth] = useState(window.innerWidth);

  useLayoutEffect(() => {
    const handleResize = () => {
      setScreenWidth(window.innerWidth);
    };

    window.addEventListener('resize', handleResize);
    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  if (screenWidth >= 992 && screenWidth < 1200) return 'xl'
  if (screenWidth >= 768 && screenWidth < 992) return 'md'
  if (screenWidth < 768) return 'sm'
};

export default useScreenSize;