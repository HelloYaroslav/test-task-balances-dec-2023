import {useAuth} from "./useAuthorization";
import {renderHook, act, waitFor} from '@testing-library/react'

describe('useAuthorization', () => {
  test.only("should successfully sign in", async () => {
    const { result } = renderHook(() => useAuth());
    await act(() => result.current.signIn({email: 'budka.teoplaya@gmail.com', password: 'Sabaka_Babaka'}));

    await waitFor(() => expect(result.current.signInStatus).toEqual('success'));
  });
  test.only("should unsuccessfully sign in", async () => {
    const { result } = renderHook(() => useAuth());
    await act(() => result.current.signIn({email: 'incorrect@email.com', password: 'Sabaka_Babaka'}));

    await waitFor(() => expect(result.current.signInStatus).toEqual('error'));
  });
})
