import {useState} from "react";
import {Balance} from "../ui/Balances/Balances";

export type Status = 'idl' | 'success' | 'error' | 'pending';

interface Response {
  status: Status;
  data?: Balance[]
}

export const useGetCurrencies = () => {
  const [response, setResponse] = useState<Response>({status: 'idl', data: []});

  const getCurrencies = async (withError: boolean = false) => {
    try {
      setResponse({status: 'pending'})
      const url = withError
      ? 'https://653fb0ea9e8bd3be29e10cd4.mockapi.io/api/v1'
      : 'https://653fb0ea9e8bd3be29e10cd4.mockapi.io/api/v1/currencies'

      const response = await fetch(url, {
        method: 'GET',
      });
      if (!response.ok) {
        throw new Error('Network response was not OK');
      }
      const data = await response.json();
      setResponse({data, status: 'success'})

    } catch (error) {
      setResponse({status: 'error'})
    }
  }

  return {
    getCurrencies,
    response,
  }
}