import {useState} from "react";
import {SignInCredentials, SignUpCredentials} from "../types/types";

const INCORRECT_EMAIL = 'incorrect@email.com';
const INCORRECT_PASSWORD = 'incorrect-password';

export type Status = 'idl' | 'success' | 'error' | 'pending';

const delay = (time: number) => new Promise((resolve) => setTimeout(resolve, time));

export const useAuth = () => {
  const [signInStatus, setSignInStatus] = useState<Status>('idl')
  const [signUpStatus, setSignUpStatus] = useState<Status>('idl')

  const signIn = async (props: SignInCredentials) => {

   setSignInStatus('pending')
   await delay(2000)

   if (props.email === INCORRECT_EMAIL || props.password === INCORRECT_PASSWORD) setSignInStatus('error') // 401
   else setSignInStatus('success')
  }

  const signUp = async (props: SignUpCredentials) => {
   setSignUpStatus('pending')
   await delay(2000)

   if (props.email === INCORRECT_EMAIL) setSignUpStatus('error') // 409
   else {
     setSignUpStatus('success')
     setSignInStatus('success')
   }
  }

  return {
    signIn,
    signUp,
    signInStatus,
    signUpStatus,
    isSignedIn: signInStatus === 'success',
  }
}