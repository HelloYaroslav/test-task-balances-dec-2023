import {Authentication} from "./Authentication/Authentication";
import {Registration} from "./Registration/Registration";

export {
  Authentication,
  Registration,
}