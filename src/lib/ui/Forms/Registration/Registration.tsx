import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import styles from '../styles.module.css'
import z from 'zod'
import {useFormik} from 'formik';
import React, {useEffect} from "react";
import {Card} from "react-bootstrap";
import {Link} from "react-router-dom";
import {RegistrationFormValues} from "../../../types/types";

export const FormSchema = z.object({
  email: z.string().email({message: "Invalid email address"}),
  password: z.string().min(6, {
    message: "Must be 6 or more characters long",
  }),
  repeatPassword: z.string()
}).superRefine(({repeatPassword, password}, ctx) => {
  if (repeatPassword !== password) {
    ctx.addIssue({
      code: "custom",
      path: ['repeatPassword'],
      message: "The passwords did not match"
    });
  }
});

interface Props {
  onSubmit: (values: RegistrationFormValues) => void;
  isLoading?: boolean;
  errors?: Partial<RegistrationFormValues>
}

export const Registration = (props: Props) => {
  const {errors} = props

  const handleValidate = (values: RegistrationFormValues) => {
    const result = FormSchema.safeParse(values)
    if (!result.success) {
      return result.error.flatten().fieldErrors
    }
  }

  const formik = useFormik({
    onSubmit: props.onSubmit,
    validate: handleValidate,
    initialValues: {
      email: '',
      password: '',
      repeatPassword: '',
    }
  })

  useEffect(() => {
    if (errors) {
      formik.setErrors(errors)
    }

  }, [errors])

  return (
    <div className="d-flex flex-row justify-content-center mt-5">
      <Card className={styles.card}>
        <Card.Title className="text-center">
          Registration Form
        </Card.Title>
        <Card.Body>
          <Form noValidate onSubmit={formik.handleSubmit}>
            <Form.Group className="mb-3" controlId="email">
              <Form.Label>Email address</Form.Label>
              <Form.Control
                type="input"
                placeholder="Enter email"
                value={formik.values.email}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                isInvalid={formik.touched.email && !!formik.errors.email}/>
              <Form.Control.Feedback type="invalid">{formik.errors.email}</Form.Control.Feedback>
              {!formik.errors.email && <Form.Text className="text-muted">
                We'll never share your email with anyone else.
              </Form.Text>}
            </Form.Group>

            <Form.Group className="mb-3" controlId="password">
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Password"
                value={formik.values.password}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                isInvalid={formik.touched.password && !!formik.errors.password}/>
              <Form.Control.Feedback type="invalid">{formik.errors.password}</Form.Control.Feedback>
            </Form.Group>
            <Form.Group className="mb-3" controlId="repeatPassword">
              <Form.Label>Repeat Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Password"
                value={formik.values.repeatPassword}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                isInvalid={!!formik.errors.repeatPassword}/>
              <Form.Control.Feedback type="invalid">{formik.errors.repeatPassword}</Form.Control.Feedback>
            </Form.Group>
            <div className="d-grid gap-2">
              <Link to="/authentication">Click if you already have an account</Link>
              <Button onClick={() => formik.setTouched({email: true, password: true, repeatPassword: true})}
                      variant="primary" type="submit">
                Submit
              </Button>
            </div>
          </Form>
        </Card.Body>
      </Card>
    </div>
  );
}