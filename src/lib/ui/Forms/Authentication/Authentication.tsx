import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import styles from '../styles.module.css'
import {useFormik} from 'formik';
import React, {useEffect} from "react";
import {Card} from "react-bootstrap";
import {Link} from "react-router-dom";
import {AuthenticationFormValues} from "../../../types/types";

interface Props {
  onSubmit: (values: AuthenticationFormValues) => void;
  errors?: Partial<AuthenticationFormValues>;
  isLoading?: boolean;
}

export const Authentication = (props: Props) => {
  const {errors} = props
  const formik = useFormik({
    onSubmit: props.onSubmit,
    initialValues: {
      email: '',
      password: '',
    }
  })

  useEffect(() => {
    if (errors) {
      formik.setErrors(errors)
    }

  }, [errors])

  return (
    <div className="d-flex flex-row justify-content-center mt-5">
      <Card className={styles.card}>
        <Card.Title className="text-center">
          Sign In Form
        </Card.Title>
        <Card.Body>
          <Form noValidate onSubmit={formik.handleSubmit}>
            <Form.Group className="mb-3" controlId="email">
              <Form.Label>Email address</Form.Label>
              <Form.Control
                type="input"
                placeholder="Enter email"
                value={formik.values.email}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                isInvalid={formik.touched.email && !!formik.errors.email}/>
              <Form.Control.Feedback type="invalid">{formik.errors.email}</Form.Control.Feedback>
              {!formik.errors.email && <Form.Text className="text-muted">
                We'll never share your email with anyone else.
              </Form.Text>}
            </Form.Group>

            <Form.Group className="mb-3" controlId="password">
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Password"
                value={formik.values.password}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                isInvalid={formik.touched.password && !!formik.errors.password}/>
              <Form.Control.Feedback type="invalid">{formik.errors.password}</Form.Control.Feedback>
            </Form.Group>
            <div className="d-grid gap-1">
              <Link to="/registration">Click to create account</Link>
              <Button variant="primary" type="submit">
                {props.isLoading
                  ? <span className="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                  : 'Submit'
                }
              </Button>
            </div>
          </Form>
        </Card.Body>
      </Card>
    </div>
  );
}