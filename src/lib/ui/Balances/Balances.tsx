import Table from 'react-bootstrap/Table';
import React, {useMemo, useState} from "react";
import useScreenSize from "../../hooks/useScreenSize";
import Button from "react-bootstrap/Button";

export interface Balance {
  amount: number;
  currencyId: string
}

export interface BalanceWithDescription extends Balance{
  currency: string
}

export interface Props {
  balances: BalanceWithDescription[];
  fetchBalancesWithError: () => void,
}

export const Balances = (props: Props) => {
  const screenSize = useScreenSize();

  const {balances, fetchBalancesWithError} = props
  const [columnCount, setColumnCount] = useState(balances.length > 3 ? 3 : 1);

  const columnCountByScreen = screenSize === 'sm' ? 1 : columnCount;

  const tableHeads = useMemo(() => {
    const heads = new Array(columnCountByScreen).fill(null).map(() => {
      return (<>
          <th className="text-start">Currency</th>
          <th className="text-end">Balance</th>
        </>
      )
    })
    return heads
  }, [columnCountByScreen])


  const tableRows = useMemo(() => {
    const rows: Array<Array<React.ReactElement>> = new Array(Math.ceil(balances.length / columnCountByScreen)).fill(null).map(() => []) // empty table rows

    balances.map(({amount, currency}, index) => { // populate each row
      const targetRow = Math.floor(index / columnCountByScreen)
      const record = (
        <>
          <td className="text-start">{currency}</td>
          <td className="text-end">{amount}</td>
        </>)
      rows[targetRow].push(record)

      if (balances.length === index + 1 && balances.length < rows.length * columnCountByScreen) { // autofill last row empty cells
        const autoCreatedCells = new Array(rows.length * columnCountByScreen - balances.length).fill(<>
          <td/>
          <td/>
        </>)
        rows[targetRow].push(...autoCreatedCells)
      }
    })
    return rows.map((row, index) => <tr key={index}>{row}</tr>)
  }, [balances, columnCountByScreen])


  return (
    <div className="p-1">
      <div className="d-none d-md-flex flex-row-reverse py-2 gap-3">
        <button onClick={() => setColumnCount(columnCount + 1)} type="button" className="btn btn-info">Increase</button>
        <button onClick={() => columnCount > 1 && setColumnCount(columnCount - 1)} type="button"
                className="btn btn-light">Decrease
        </button>
        <span className="my-auto"> Columns </span>
        <Button className="me-5" variant="link" onClick={fetchBalancesWithError} >GET CURRENCIES WITH ERROR</Button>
      </div>
      <div className="table-responsive">
        <Table striped hover variant="dark">
          <thead>
          <tr>
            {tableHeads}
          </tr>
          </thead>
          <tbody>
          {tableRows}
          </tbody>
        </Table>
      </div>
    </div>
  );
}