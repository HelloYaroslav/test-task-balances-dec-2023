import {render, screen, waitFor} from '@testing-library/react'
import renderer from 'react-test-renderer';
import userEvent from '@testing-library/user-event'
import '@testing-library/jest-dom'
import {Balances} from './Balances'
import {Props} from "./Balances";

const props: Props = {
  balances: [{
    currency: 'EUR',
    currencyId: '1',
    amount: 934,
  }],
  fetchBalancesWithError: jest.fn(),
}

test('loads and displays greeting', async () => {
  render(<Balances {...props}/>)

  expect(screen.getAllByText('Currency')).toHaveLength(1)

  userEvent.click(screen.getByText('Increase'))

  await waitFor(() => expect(screen.getAllByText('Currency')).toHaveLength(2))
})

test('component renders correctly', () => {
  const tree = renderer
    .create(<Balances {...props}/>)
    .toJSON();
  expect(tree).toMatchSnapshot();
});