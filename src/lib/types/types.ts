export interface SignInCredentials {
  email: string;
  password: string;
}

export interface SignUpCredentials {
  email: string;
  password: string;
  repeatPassword: string;
}

export interface AuthenticationFormValues {
  email: string;
  password: string;
}

export interface RegistrationFormValues {
  email: string;
  password: string;
  repeatPassword: string;
}