import { redirect, useNavigate } from "react-router-dom";
import React, {useContext, useEffect} from "react";
import {AuthContext} from "../../../App";

interface Props {
  children: React.ReactElement
}

export const Page = ({children}: Props) => {
  const auth = useContext(AuthContext);
  const navigate = useNavigate()

  useEffect(() => {
    if(!auth?.isSignedIn){
      navigate('/authentication', { replace: true})
    }
  })
  if(!auth?.isSignedIn) return null
  return children
}
