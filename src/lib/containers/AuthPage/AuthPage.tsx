import React, {useContext, useEffect} from "react";
import {AuthContext} from "../../../App";
import {useAuth} from "../../hooks/useAuthorization";
import {useNavigate} from "react-router-dom";
import {AuthenticationFormValues} from "../../types/types";

interface ChildrenProps {
  AuthPage: React.ReactElement;
}

interface AuthPagesProps {
  signIn: ReturnType<typeof useAuth>['signIn'],
  signUp: ReturnType<typeof useAuth>['signUp'],
  isLoading?: boolean,
  errors?: Partial<AuthenticationFormValues>
}

interface Props {
  signIn?: boolean;
  signUp?: boolean;
  children: (props: ChildrenProps) => React.ReactElement
  renderAuthPage: (props: AuthPagesProps) => React.ReactElement
}

export const AuthPage = ({children, renderAuthPage, signIn, signUp}: Props) => {
  const auth = useContext(AuthContext);
  const navigate = useNavigate()

  const errors: AuthPagesProps["errors"] = (() =>{
    if(signIn && auth.signInStatus === 'error')
    return {
      email: 'Invalid Email Or Password',
    }
    if(signUp && auth.signUpStatus === 'error')
    return {
      email: 'This email is already registered',
    }
  })()
  useEffect(() => {
    if(auth?.isSignedIn){
      navigate('/', { replace: true})
    }
  })
  if(auth?.isSignedIn) return null

  const AuthPage = renderAuthPage({
    signIn: auth.signIn,
    signUp: auth.signUp,
    isLoading: auth.signUpStatus === 'pending' || auth.signInStatus === 'pending',
    errors
  })

  return children({AuthPage})
}
