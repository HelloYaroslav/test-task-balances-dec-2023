import React, {useEffect, useMemo} from "react";
import {Balance, BalanceWithDescription} from "../../ui/Balances/Balances";
import {CURRENCIES_DICTIONARY} from "../../utils/dictionaries/dictionaries";
import {useGetCurrencies} from "../../hooks/useGetCurrencies";

interface ChildrenProps {
  Balances: React.ReactElement;
}

interface BalancesProps {
  balances: BalanceWithDescription[],
  fetchBalancesWithError: () => void,
}

interface Props {
  children: (props: ChildrenProps) => React.ReactElement
  renderBalances: (props: BalancesProps) => React.ReactElement
  renderErrorPage: (getCurrencies: () => void) => React.ReactElement // 404, not fount, etc.
  renderLoading: () => React.ReactElement
}

export const Balances = ({children, renderBalances, renderErrorPage, renderLoading}: Props): React.ReactElement => {
  const { response, getCurrencies} = useGetCurrencies()

  useEffect(() => {
    if(response.status === 'idl') getCurrencies()
  }, [response.status, getCurrencies])

  const describedBalances = useMemo(() => response.data?.map((balance) => ({
    ...balance,
    currency: CURRENCIES_DICTIONARY[balance.currencyId]
  })), [response.data])

  const fetchBalancesWithError = () => getCurrencies(true)

  if(response.status === 'idl' || response.status === 'pending') return renderLoading()
  if(response.status === 'error') return renderErrorPage(getCurrencies)

  return children({
    Balances: renderBalances({ balances: describedBalances || [], fetchBalancesWithError})
  })
}

